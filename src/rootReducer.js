import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from 'redux-form'
import UserApplicationReducer from "../src/Screens/UserApplication/UserApplication.reducer"

//import workbookReducer from "../screens/workbook/workbook.reducer";


export default combineReducers({
  routing: routerReducer,
  form : formReducer,
  userApplication:UserApplicationReducer
  //workbook: workbookReducer,
});