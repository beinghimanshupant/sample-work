import React from "react"
import HomeRender from "./HomeRender"

class HomeContainer extends React.Component {
    componentDidMount(){
        this.getHomeScreenData()
    }

    getHomeScreenData =()=>{

    }

    goToProfile =()=>{
        this.props.history.push('/edit')
    }
    render() {
        return (<HomeRender
            getHomeScreenData={this.getHomeScreenData}
            goToProfile={this.goToProfile}
            />)
    }
}

export default HomeContainer