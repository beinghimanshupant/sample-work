import React from "react"
import ReactPullToRefresh from "react-pull-to-refresh"
import Button from "react-bootstrap/Button"
import "./home.styles.css"

const HomeRender =({getHomeScreenData,goToProfile})=>(<div>
<ReactPullToRefresh
 onRefresh={getHomeScreenData}
 style={{
    textAlign: 'center'
  }}
>
<h3>Pull down to refresh</h3>
</ReactPullToRefresh>
<div className="highlighted-image">
<Button
variant = "link"
onClick={()=>goToProfile()}>
Watch Trailer

</Button>
</div>
</div>
)

export default HomeRender