import React from "react"
import { connect } from "react-redux";
import {getUserDetails} from "../UserApplication/UserApplication.selectors"
class EditDetails extends React.Component{

    render(){
        const {userDetails} = this.props
        return(<div className="edit-details">
        First Name : {userDetails.firstName}
        {" "} 
        {/* todo need to remove empty string */}
        Last Name : {userDetails.lastName}

        </div>)
    }

}
const mapStateToProps = state => ({
    userDetails: getUserDetails(state)
})
const mapDispatchToProps = {
    
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditDetails);