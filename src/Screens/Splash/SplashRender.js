import React from "react"
import "./splash.styles.css"

 const SplashRender=()=>(
    <div className="splash-render">
        <img src={require("../../../src/assets/images/play-button-icon.svg")} alt="icon"/>
    </div>
 )
export default SplashRender