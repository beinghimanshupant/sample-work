import React from "react"
import SplashRender from "./SplashRender"
import {getUserDetails} from "../UserApplication/UserApplication.actions"
import { connect } from "react-redux";



class SplashContainer extends React.Component{
    constructor(props)
    {
        super(props)
        this.state={isUserAlreadyRegistered:false} // Setting here for registered users
    }

    componentDidMount(){
        this.props.getUserDetails()
        if(this.state.isUserAlreadyRegistered)
        setInterval(()=>{this.props.history.push('/home')},3000)
        else
        setInterval(()=>{this.props.history.push('/application')},3000)

     
    }

    render()
    {
        return(
            <div>
                <SplashRender/>
            </div>

        )
    }
}
    const mapStateToProps = state => ({

    })
    const mapDispatchToProps = {
        getUserDetails
    }
    
    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(SplashContainer);