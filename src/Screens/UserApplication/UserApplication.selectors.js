import get from "lodash/get"
export const getUserDetails = (state) =>
    get(state,"userApplication",{})
