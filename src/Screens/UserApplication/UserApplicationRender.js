import React from "react"
import { Field, reduxForm } from 'redux-form'
import "./userapplication.styles.css"

const UserApplicationRender = props => {
    const { handleSubmit, pristine, reset, submitting, saveUserApplication, isFieldEditable } = props
    return (
        <div className="user-application-render">
      {isFieldEditable &&  <div className="message">
        Hey! What should I call you?
        </div>}
      <form onSubmit={handleSubmit((value)=>{saveUserApplication(value)})}>
        <div>
          <label>First Name</label>
          <div>
            <Field
              name="firstName"
              component="input"
              type="text"
              placeholder="First Name"
            />
          </div>
        </div>
        <div>
          <label>Last Name</label>
          <div>
            <Field
              name="lastName"
              component="input"
              type="text"
              placeholder="Last Name"
            />
          </div>
        </div>
        
        <div>
          <button type="submit" disabled={pristine || submitting}>
            Submit
          </button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>
            Clear Values
          </button>
        </div>
      </form>
      </div>
    )
  }
  
  export default reduxForm({
    form: 'userApplication'
  })(UserApplicationRender)