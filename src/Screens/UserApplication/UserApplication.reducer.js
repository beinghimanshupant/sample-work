const initialState = {
    firstName: null,
    lastName: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SAVE":
            const { firstName, lastName } = action.payload
            return {
                ...state,
                firstName,
                lastName
            }

            case "GET":
            const {firstName:fname, lastName:lname} = action.payload
            return {
                ...state,
                firstName:fname ,
                lastName: lname
            }

        default:
            return state
    }
}
export default reducer;