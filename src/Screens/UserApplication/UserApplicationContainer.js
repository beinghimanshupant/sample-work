import React from "react"
import UserApplicationRender from "./UserApplicationRender"
import { connect } from "react-redux";
import { saveUserDetailsToStore } from "./UserApplication.actions"
import { getUserDetails } from "./UserApplication.selectors"

class UserApplicationContainer extends React.Component {

    saveUserApplication = (value) => {
        this.props.saveUserDetailsToStore(value)
    }

    componentDidUpdate(prevProps) {
        const { userDetails,history } = this.props
        if (prevProps.userDetails !== userDetails) {
            history.push('/home')
        }
    }

    render() {
        const {isFieldEditable,userDetails} = this.props
        return (<UserApplicationRender
            saveUserApplication={this.saveUserApplication}
            isFieldEditable={isFieldEditable!==undefined ? isFieldEditable:true}
            userDetails={userDetails}
        />)
    }
}

const mapStateToProps = state => ({
    userDetails: getUserDetails(state)
})
const mapDispatchToProps = {
    saveUserDetailsToStore
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserApplicationContainer);