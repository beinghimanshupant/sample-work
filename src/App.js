import React, { Component } from "react";
import PropTypes from "prop-types";
import { Route,Router } from "react-router-dom";
import SplashContainer from "../src/Screens/Splash/SplashContainer";
import UserApplicationContainer from "../src/Screens/UserApplication/UserApplicationContainer"
import HomeContainer from "../src/Screens/Home/HomeContainer"
import EditDetails from "../src/Screens/EditDetails/EditDetails"

class App extends Component {
  render() {
    const { history } = this.props;

    return (
      <Router history={history}>
          <Route
          exact
            path='/'
            component={SplashContainer}
          />
           <Route
           exact
            path='/application'
            component={UserApplicationContainer}
          />
          <Route
           exact
            path='/home'
            component={HomeContainer}
          />
           <Route
           exact
            path='/edit'
            component={EditDetails}
          />
          
      </Router>
    );
  }
}

App.propTypes = {
  history: PropTypes.object.isRequired
};

export default App;